"""
compute_trim
    - Chapter 5 assignment for Beard & McLain, PUP, 2012
    - Update history:
        12/29/2018 - RWB
"""
from typing import Any

import numpy as np
import numpy.typing as npt
from mav_sim.chap3.mav_dynamics_euler import (
    IND_EULER,
    derivatives_euler,
    euler_state_to_quat_state,
    quat_state_to_euler_state,
)
from mav_sim.chap4.mav_dynamics import forces_moments, update_velocity_data
from mav_sim.message_types.msg_delta import MsgDelta
from mav_sim.tools import types
from scipy.optimize import Bounds, minimize


def compute_trim(state0: types.DynamicState, Va: float, gamma: float, R: float = np.inf) -> tuple[types.DynamicState, MsgDelta]:
    """Compute the trim equilibrium given the airspeed and flight path angle

    Args:
        mav: instance of the mav dynamics class
        Va: air speed
        gamma: flight path angle

    Returns:
        trim_state: The resulting trim trajectory state
        trim_input: The resulting trim trajectory inputs
    """
    # Convert the state to euler representation
    state0_euler = quat_state_to_euler_state(state0)

    # Calculate the trim
    trim_state_euler, trim_input = compute_trim_euler(state0=state0_euler, Va=Va, gamma=gamma, R=R)

    # Convert and output the returned value
    trim_state = euler_state_to_quat_state(trim_state_euler)
    return trim_state, trim_input

def compute_trim_euler(state0: types.DynamicStateEuler, Va: float, gamma: float, R: float) \
    -> tuple[types.DynamicStateEuler, MsgDelta]:
    """Compute the trim equilibrium given the airspeed, flight path angle, and radius

    Args:
        mav: instance of the mav dynamics class
        Va: air speed
        gamma: flight path angle
        R: Radius

    Returns:
        trim_state: The resulting trim trajectory state
        trim_input: The resulting trim trajectory inputs
    """
    # define initial state and input
    delta0 = MsgDelta(elevator=0., aileron=0., rudder=0., throttle=0.5)
    x0 = np.concatenate((state0, delta0.to_array()), axis=0)

    # define equality constraints

    # Define the bounds
    eps = 1e-12 # Small number to force equality constraint to be feasible during optimization (bug in scipy)
    # -lower             pn                       pe                             pd

    # -upper             pn                       pe                             pd

    # solve the minimization problem to find the trim states and inputs
    # res = minimize(trim_objective_fun_euler, x0, method='SLSQP', args=(Va, gamma, R), bounds=Bounds(lb=lb, ub=ub),
    #                constraints=cons, options={'ftol': 1e-10, 'disp': False})

    # extract trim state and input and return
    # trim_state = np.array([res.x[0:12]]).T
    # trim_input = MsgDelta(elevator=res.x.item(12),
    #                       aileron=res.x.item(13),
    #                       rudder=res.x.item(14),
    #                       throttle=res.x.item(15))

    return state0, delta0

def trim_objective_fun_euler(x: npt.NDArray[Any], Va: float, gamma: float, R: float) -> float:
    """Calculates the cost on the trim trajectory being optimized

    Objective is the norm of the desired dynamics subtract the actual dynamics (except the x-y position variables)

    Args:
        x: current state and inputs combined into a single vector
        Va: relative wind vector magnitude
        gamma: flight path angle

    Returns:
        J: resulting cost of the current parameters
    """
    # Extract the state and input
    state = x[0:12]
    delta = MsgDelta(elevator=x.item(12),
                     aileron=x.item(13),
                     rudder=x.item(14),
                     throttle=x.item(15))

    # Calculate the desired trim trajectory dynamics


    # Calculate forces

    # Calculate the dynamics based upon the current state and input (use euler derivatives)

    # Calculate the difference between the desired and actual


    # Calculate the square of the difference (neglecting pn and pe)
    # Put an emphasis on the \dot{psi} to get to desired radius
    return 0.
