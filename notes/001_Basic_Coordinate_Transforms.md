# Basic Coordinate Transforms

## Refrecence Frames

![Reference Frames](figures/refrence_frames.jpg)

## Pure Translation

* Pure translation the axes are parallel and the origin is shifted
* first we need to define a vector between the two frames
* $v_{iv}^i$: vector from frame i to frame v expressed in frame i
* used to convert a from frame v to frame i
* Example 1: $v_{iv}^i = [3, 2]^T$
* $p^i = v^i_{iv} + p^v= [3, 2]^T +[1, 3]^T = [4, 5]^T$
* Example 2: $q_{iv}^i v^i_{iv} + q^v= [3, 2]^T +[-1, 2]^T = [2, 4]^T$
![Pure Translation](figures/pure_translation.jpg)`
* ### Pure Translations do not change vectors
* $p^i = [1, 3]^T, q_i = [0, 0]^T$ u = pi q
* $u^i = p^i - q^i = [1, 3]^T$ 
* Looking at v-Frame $F^v$
* $u^v = p^v - q^v = [-2, 1]^T + [3, 2]^T = [1, 3]^T$
* $p^v = v_{iv}^i + p^i, q^v = v^v_{vi} + p^i$
* = $v_vi^v + p^i - (v_{vi}^v+ q^i)= p^i-q^i$

![Pure Translation Vectors](figures/vector_pure_translation.jpg)
## $2^{\text{nd}}$ Transform: Pure 2D Rotation
* $i_a^b$: unit vector pointing along the a-frame x-axis described in frame b
* $j_a^b$: unit vector pointing along the a-frame y-axis described in frame b
* $k_a^b$: unit vector pointing along the a-frame z-axis described in frame b
* $i_a^a = [1, 0]^T, j_a^a= [0, 1]^T$ 
* $theta = \frac{\pi}{4}$
* $i_a^b = \frac{1}{2}[\sqrt{2}, \sqrt{2}]^T, j_a^b = \frac{1}{2}[\sqrt{2}, \sqrt{2}]^T$

![Pure Rotation](figures/pure_rotation.jpg)

* Origin Points of frames are co-located
* Recall directions cosine matrices
    * just describe the elementary vectors
    * $R_a^b$: Rotation from frame a to frame b
$$ R_a= [i_a^b, j_a^b] = \frac{1}{2}
\begin{bmatrix}
\sqrt{2} & \sqrt{2} \\
-\sqrt{2} & \sqrt{2}
\end{bmatrix} 
= R(\theta) =
\begin{bmatrix}
c(\theta) & s(\theta) \\
-s(\theta) & c(\theta) \\
\end{bmatrix} 
 $$
 * Example 1: $p^b = 2i_b^b = [2, 0]^T, p^a = [\sqrt{2}, \sqrt{2}]^T$
    * $p^b = R_a^b p^a = \frac{\sqrt{2}}{2}
    \begin{bmatrix}
    1, 1 \\
    -1, 1
    \end{bmatrix}
    (\sqrt{2}[1, 1]^T) = \frac{2}{0}$
    * $R_b^a = R(-\frac{\pi}{4})$
    * $R_a^b = (R_b^a)^T$
    * $R_a^b * R_b^a = I$
    * $(R_a^b)^{-1} = R_b^a$
## Note - Two Types of Rotation
* Passive: Fixed point, rotating frame
    * Previous examples were this
* Active: Fixed frame, rotating point
![Active Rotation](figures/active_rotation.jpg)
## 2D Rotation: Two Rotations
* We know the two rotation angles and have a known point in frame a- how do we get it to frame c?
    * Total rotation: $\theta = \theta_1 + \theta_2$
    * $R(\theta) = R(\theta_1+ \theta_2)$
    * = $\begin{bmatrix}
    c(\theta_1 + \theta_2) & s(\theta_1 + \theta_2) \\
    -s(\theta_1 + \theta_2) & c(\theta_1 + \theta_2) \\
    \end{bmatrix}$
    * $R(\theta_1)R(\theta_2) = R(\theta_2) + R(\theta_1)$
    * rotations are communitive meaning order does not matter 2D! only
* step by step
    * $p^a$ is known
        * $p^b = R_a^b p^a = R(\theta_1)p^a$
        * $p^c = R_b^c p^b = R(\theta_2)R(\theta_1)p^a$
        * = $p_c = R_b^c*R_a^b*p^a$
    * if $R_c^d$ is known
        * $p^d = R_c^d * R_b^c * R_a^b * p^a$

![Two Rotations](figures/2d_rotation.jpg)

## Facts
* 2d rotation matrices are commutative (3d are not)
* 2d rotation matrices belong to the 
## $3^{\text{3d}}$ Rotation: 3d Rotation
![3d Rotation](figures/3d_rotation.jpg)
* $R_a^b = [i_a^b, j_a^b, k_a^b], i_a^a = [1, 0, 0]^T, j_a^a = [0, 1, 0], k_a^a = [0, 0, 1]^T$
* use positive right-hand rotations about an axis
$$x: (\phi) = R_x(\phi) = 
\begin{bmatrix}
1, 0, 0  \\
0, c(\phi), s(\phi) \\
0, -s(\phi), c(\phi) \\
\end{bmatrix}$$

$$ y: (\theta) = R_y(\theta) =
\begin{bmatrix}
c(\theta), 0, -s(\theta) \\
0, 1, 0 \\
s(\theta), 0, c(\theta) \\
\end{bmatrix}
$$

$$ z: (\psi) = R_z(\psi) =
\begin{bmatrix}
c(\psi), s(\psi), 0 \\
-s(\psi), c(\psi), 0 \\
0, 0, 1 \\
\end{bmatrix}
$$
## 3d Rotation: Euler Angles
### Becarful of order there are different ways to formulate euler matrix
![3d Rotation Euler](figures/3d_rotation_euler.jpg)
## 3D Rotation Properties
* 3d rotation matrices are not commutative
* 3d rotations belong to the special orthogonal group $SO(3)$
* the 3d inverse is same as 2d inverse
3d rotation matrices can be chained in the same fashion as 2d rotation matrices
* take extreme care when you are given euler angles as there are 12 combinations foro euler angles
* ### Major difference between 2d and 3d rotations
    * 2d has on degree of freedom 3d has 2
    * 3d is not commutative
### 4th Transformation: 3d Rotations & Translations
* This group of transforms are known as the special euclidean group $SE(3) = \{(p,R) | p \in \mathbb{R}^3, R \in SO(3)\}$
* Example
![4th Transformation](figures/4th_transformation.jpg)

* we have $p^b$ and we want $p^a$
* first rotatie to frame parallel to frame a
* then add the dfference vector
* $p^a = R_b^a * p^b + v_{ab}^b$
* for uav:
    1) $p^b \rightarrow p^v: p^v = R_b^v p^b$ body frame to vehicle frame
    2) $p^v \rightarrow p^i p^i = v_{iv}^i + p^v: vehicle frame to inertial frame


